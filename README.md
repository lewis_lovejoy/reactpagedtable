# REACT based pageable table with access to REST end-point

This project was bootstrapped with [Create React App]

## Simplifications (due to time constraint)

- I haven't used Redux etc for the data fetch - I've done it directly in the component. For this simple example this isn't a
  a major issue,but for more complex systems this becomes un-maintainable very quickly.

- For readability every source file is in a single directory - again not the best for maintainable code, 
  but ok in a simple example - so all SASS files, source files, tests and images are all in one place

- I haven't tidied all the create-react-app files created - so there are a number of boiler plate files (like logo.svg) that
  are still there

- I've done bare minimum responsive stuff and only basic React (ignoring cross browser issues)

- I've also ignored all SEO issues for the page (no Helmet)

- Test coverage is high (over 96% ignoring boilerplate files), but the tests aren't great. In particular, error states and edge-cases 
  are not covered by the tests (or the spec.). Also, some assumptions that the external components
  used work - e.g. no real tests of the query parameter processing 
  (on the assumption react-router-dom works)

- I've done everything in a single commit as this is a simple example - I would normally
  split into multiple commits for different 'features' - but I coded this all as one 'feature'

## Interpretations

- I've read 'prints out the results as a list' as 'displays the results in a table onscreen', this is mainly
because the returned data has a structure that fits a table better than a list - obviously really easy to revert to a list
if the format of the list is well-defined (e.g `${Title} - ${Author}....` ). Also, might as well use 
bootstrap table as its there...

- The spec doesn't specifically ask for a 'number of items on page' dropdown/selector - so I haven't used one
and have just used the default number of items per page (i.e. 20). So the 'page selector' is JUST for
page number (and this is reflected in the query string)

- The example return value from the REST api on the spec has 'book_publication_city:' in the JSON twice.
I'm assuming that's a typo - if it's not, then the 'JSON' needs handling in a different way to the
built in methods (the code works, so I guess this assumption is true)



