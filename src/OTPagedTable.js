import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Alert, Spinner, Button, Card, InputGroup, FormControl } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import axios from 'axios';
import queryString from 'query-string';

import OTTable from './OTTable';
import OTPagination from './OTPagination';

function OTPagedTable({ location }) {
  const queryStr = queryString.parse(location.search);
  const itemsPerPage = 20;
  const { page = '1', query = '' } = queryStr;

  const [data, setData] = useState({ books: [] });
  const [numPages, setNumPages] = useState(1);
  const [currentQuery, setCurrentQuery] = useState(query);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);

      try {
        const result = await axios.post('http://nyx.vima.ekt.gr:3000/api/books', {
            page,
            itemsPerPage,
            filters: currentQuery !=='' ? [{type: "all", values: [currentQuery]}] : undefined
        });
        setData(result.data);
        setNumPages(Math.floor(result.data.count / itemsPerPage) + 1);
      } catch (error) {
        setIsError(true);
      }
      setIsLoading(false);
    };

    fetchData();
  }, [page, query]);

  if (isError) {
    return (
        <Alert variant="danger">
          Something went wrong ...
        </Alert>
    );
  }

  if (isLoading) {
    return (
        <Card body>
          <Button variant="primary" disabled>
            <Spinner
                as="span"
                size="sm"
                animation="border"
                role="status"
                aria-hidden="true"
            />
            Loading, please wait...
          </Button>
        </Card>
    );
  }

  return (
      <Card body>
        <Container fluid>
          <Row>
            <Col md lg="6">
              <InputGroup className="mb-3">
                <FormControl
                    placeholder="Query"
                    aria-label="Query"
                    aria-describedby="basic-addon2"
                    value={currentQuery}
                    onChange={event => setCurrentQuery(event.target.value)}
                />
                <InputGroup.Append>
                  <LinkContainer to={`/?page=1&query=${currentQuery}`}>
                    <Button variant="outline-secondary">
                      Search
                    </Button>
                  </LinkContainer>
                </InputGroup.Append>
              </InputGroup>
            </Col>
            <Col md lg="6" className="d-flex justify-content-lg-end">
              <OTPagination page={parseInt(page)} lastPage={numPages} />
            </Col>
          </Row>
          <Row>
            <Col>
              <OTTable data={data.books} />
            </Col>
          </Row>
        </Container>
      </Card>
  );
}

export default OTPagedTable;
