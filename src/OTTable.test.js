import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

import OTTable from './OTTable';

const testData = [{
  id: 1,
  book_title: 'some data',
  book_author: 'some author',
  book_publication_city: 'some city',
  book_publication_country: 'some country',
  book_publication_year: ' some year',
  book_pages: 'some pages',
}, {
  id: 1,
  book_title: 'some data2',
  book_author: 'some author2',
  book_publication_city: 'some city2',
  book_publication_country: 'some country2',
  book_publication_year: ' some year2',
  book_pages: 'some pages2',
}];

test('blank table has normal headings to display on table', async () => {
  render(<OTTable data={[{}]} />, { wrapper: MemoryRouter });

  expect(screen.getByText('Id')).toBeInTheDocument();
  expect(screen.getByText('Title')).toBeInTheDocument();
  expect(screen.getByText('Author')).toBeInTheDocument();
  expect(screen.getByText('City')).toBeInTheDocument();
  expect(screen.getByText('Country')).toBeInTheDocument();
  expect(screen.getByText('Year')).toBeInTheDocument();
  expect(screen.getByText('# Pages')).toBeInTheDocument();
});

test('table shows data passed to it', async () => {
  render(<OTTable data={testData} />, { wrapper: MemoryRouter });

  expect(screen.getByText('some data')).toBeInTheDocument();
  expect(screen.getByText('some author')).toBeInTheDocument();
  expect(screen.getByText('some city')).toBeInTheDocument();
  expect(screen.getByText('some country')).toBeInTheDocument();
  expect(screen.getByText('some year')).toBeInTheDocument();
  expect(screen.getByText('some pages')).toBeInTheDocument();

  expect(screen.getByText('some data2')).toBeInTheDocument();
  expect(screen.getByText('some author2')).toBeInTheDocument();
  expect(screen.getByText('some city2')).toBeInTheDocument();
  expect(screen.getByText('some country2')).toBeInTheDocument();
  expect(screen.getByText('some year2')).toBeInTheDocument();
  expect(screen.getByText('some pages2')).toBeInTheDocument();

  const rows = screen.getByText('some data').closest('table').querySelectorAll('tr');
  expect(rows).toHaveLength(testData.length + 1); // including header
});
