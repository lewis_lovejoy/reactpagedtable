import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

import { App } from './App';
import OTPagination from './OTPagination';

test('normally shows links for pages 1 to 6 for 6 pages', async () => {
  render(<OTPagination page={1} lastPage={6}/>, { wrapper: MemoryRouter });

  expect(screen.getByText('1')).toBeInTheDocument();
  expect(screen.getByText('1')).toContainHTML('href="/?page=1"');
  expect(screen.getByText('1')).not.toContainHTML('disabled=""');
  expect(screen.getByText('2')).toBeInTheDocument();
  expect(screen.getByText('2')).toContainHTML('href="/?page=2"');
  expect(screen.getByText('2')).not.toContainHTML('disabled=""');
  expect(screen.getByText('3')).toBeInTheDocument();
  expect(screen.getByText('3')).toContainHTML('href="/?page=3"');
  expect(screen.getByText('3')).not.toContainHTML('disabled=""');
  expect(screen.getByText('4')).toBeInTheDocument();
  expect(screen.getByText('4')).toContainHTML('href="/?page=4"');
  expect(screen.getByText('4')).not.toContainHTML('disabled=""');
  expect(screen.getByText('5')).toBeInTheDocument();
  expect(screen.getByText('5')).toContainHTML('href="/?page=5"');
  expect(screen.getByText('5')).not.toContainHTML('disabled=""');
  expect(screen.getByText('6')).toBeInTheDocument();
  expect(screen.getByText('6')).toContainHTML('href="/?page=6"');
  expect(screen.getByText('6')).not.toContainHTML('disabled=""');
})

test('first/prev are disabled for only 6 pages when on page 1', async () => {
  render(<OTPagination page={1} lastPage={6}/>, { wrapper: MemoryRouter });

  expect(screen.getByTestId('first')).toContainHTML('disabled=""');
  expect(screen.getByTestId('prev')).toContainHTML('disabled=""');
})

test('next/last are disabled for only 6 pages when on page 6', async () => {
  render(<OTPagination page={6} lastPage={6}/>, { wrapper: MemoryRouter });

  expect(screen.getByTestId('next')).toContainHTML('disabled=""');
  expect(screen.getByTestId('last')).toContainHTML('disabled=""');
})

test('one page only shows one link', async () => {
  render(<OTPagination page={1} lastPage={1}/>, { wrapper: MemoryRouter });

  expect(screen.getByText('1')).toBeInTheDocument();
  expect(screen.getByText('1')).toContainHTML('href="/?page=1"');
  expect(screen.getByText('1')).not.toContainHTML('disabled=""');
  expect(screen.queryByText('2')).not.toBeInTheDocument();
  expect(screen.getByTestId('first')).toContainHTML('disabled=""');
  expect(screen.getByTestId('prev')).toContainHTML('disabled=""');
  expect(screen.getByTestId('next')).toContainHTML('disabled=""');
  expect(screen.getByTestId('last')).toContainHTML('disabled=""');
})

test('first/prev/next/last are enabled for 10 pages when on page 3', async () => {
  render(<OTPagination page={3} lastPage={10}/>, { wrapper: MemoryRouter });

  expect(screen.getByTestId('first')).not.toContainHTML('disabled=""');
  expect(screen.getByTestId('prev')).not.toContainHTML('disabled=""');
  expect(screen.getByTestId('next')).not.toContainHTML('disabled=""');
  expect(screen.getByTestId('last')).not.toContainHTML('disabled=""');
})
