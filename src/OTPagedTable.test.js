import { render, screen, act } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import axios from 'axios';

import OTTable from './OTTable';
import OTPagination from './OTPagination';
import OTPagedTable from './OTPagedTable';

jest.mock('axios');
jest.mock('./OTTable');
jest.mock('./OTPagination');

const testDataFromSpec = {
  books: [{
    book_author: ['xxxxxx'],
    book_publication_city: 'xxxxxx',
    book_publication_country: 'xxxxxx',
    book_publication_year: 'xxxxxx',
    book_pages: 23,
    book_title: 'xxxxxx',
    id: 1
  }],
  count: 2000
};

beforeEach(()=>{
  OTTable.mockImplementation(() => { return "Table Renders Here"; });
  OTPagination.mockImplementation(() => { return "Pagination Renders Here"; });
})

test('Paged Table uses axios to get data', async () => {
  axios.post.mockImplementationOnce(() => Promise.resolve({data: testDataFromSpec}));

  await act(async () => {
    render(<OTPagedTable location={{}} />, { wrapper: MemoryRouter });
  });

  expect(axios.post).toHaveBeenCalledWith(
      "http://nyx.vima.ekt.gr:3000/api/books",
      {"filters": undefined, "itemsPerPage": 20, "page": "1"}
  );
});

test('Paged Table renders a table component with data', async () => {
  axios.post.mockImplementationOnce(() => Promise.resolve({data: testDataFromSpec}));

  await act(async () => {
    render(<OTPagedTable location={{}} />, { wrapper: MemoryRouter });
  });

  expect(OTTable).toHaveBeenCalledWith({ data: []}, {} );
  expect(OTTable).toHaveBeenCalledWith({ data: testDataFromSpec.books }, {});
  expect(screen.getByText('Table Renders Here')).toBeVisible();
});

test('Paged Table renders a pagination component with data', async () => {
  axios.post.mockImplementationOnce(() => Promise.resolve({data: testDataFromSpec}));

  await act(async () => {
    render(<OTPagedTable location={{}} />, { wrapper: MemoryRouter });
  });

  expect(OTPagination).toHaveBeenCalledWith({
    lastPage: 1,
    page: 1
  }, {} );
  expect(OTPagination).toHaveBeenCalledWith({
    lastPage: Math.floor( testDataFromSpec.count / 20) + 1,
    page: 1
  }, {});
  expect(screen.getByText('Pagination Renders Here')).toBeVisible();
});

test('Paged Table shows error on invalid data', async () => {
  axios.post.mockImplementationOnce(() => Promise.resolve({}));

  await act(async () => {
    render(<OTPagedTable location={{}} />, { wrapper: MemoryRouter });
  });

  expect(screen.getByText('Something went wrong ...')).toBeVisible();
});
