import React from 'react';
import { Pagination } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

function OTPagination({ page, lastPage }) {
  const numPagesToShow = Math.min(6, lastPage);
  const limitFirstPage = (p) => ((p - 3) < 1 ? 1 : (p - 3));
  const limitLastPage = (p) => ((p + 3) > (lastPage - 3) ? (lastPage - numPagesToShow + 1) : p);
  const firstPage = limitLastPage(limitFirstPage(page));

  const pageItems = Array.from({ length: numPagesToShow }, (v,i)=>(
      <LinkContainer key={i} to={`/?page=${i+firstPage}`}>
        <Pagination.Item active={page===i+firstPage}>{i+firstPage}</Pagination.Item>
      </LinkContainer>
  ));

  return (
    <Pagination size="sm">
      <LinkContainer data-testid="first" to={`/?page=1`}>
        <Pagination.First disabled={page<2}/>
      </LinkContainer>
      <LinkContainer data-testid="prev" to={`/?page=${page-1}`}>
        <Pagination.Prev disabled={page<2}/>
      </LinkContainer>

      {pageItems}

      <LinkContainer data-testid="next" to={`/?page=${page+1}`}>
        <Pagination.Next disabled={page>lastPage-1}/>
      </LinkContainer>
      <LinkContainer data-testid="last" to={`/?page=${lastPage}`}>
        <Pagination.Last disabled={page>lastPage-1}/>
      </LinkContainer>
    </Pagination>
  );
}

export default OTPagination;
