import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from 'react-router-dom';
import React from 'react';

import App from './App';

test('full app rendering/navigating', () => {
  render(<App />, { wrapper: MemoryRouter });

  expect(screen.getByText('Loading, please wait...')).toBeInTheDocument();
})
