import React from 'react';
import { Table } from 'react-bootstrap';

function OTTable({ data }) {
  return (
      <>
        <Table striped bordered hover size="sm" data-testid="ottable">
          <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Author</th>
            <th>City</th>
            <th>Country</th>
            <th>Year</th>
            <th># Pages</th>
          </tr>
          </thead>
          <tbody>
            {data.map((item,i) => (
                <tr key={i}>
                  <td>{item.id}</td>
                  <td>{item.book_title}</td>
                  <td>{item.book_author}</td>
                  <td>{item.book_publication_city}</td>
                  <td>{item.book_publication_country}</td>
                  <td>{item.book_publication_year}</td>
                  <td>{item.book_pages}</td>
                </tr>
            ))}
          </tbody>
        </Table>
      </>
  );
}

export default OTTable;
