import { BrowserRouter as Router, Route } from 'react-router-dom';

import OTPagedTable from './OTPagedTable';

function App() {
  return (
      <section className="App">
        <Router>
          <Route exact path="/" component={OTPagedTable} />
        </Router>
      </section>
  );
}

export default App
